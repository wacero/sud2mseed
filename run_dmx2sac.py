
import multiprocessing as mp
import sud2mseed_utils
import time
import logging


logging.basicConfig(format='%(message)s', filename=("./dmx2sac_error_special_dmx_99.log"),level=logging.ERROR)
main_data_path="e:\\salida_dmx\\dmx_especial\\99\\"
#main_data_path="f:\\salida_dmx\\dmx_especial\\"
dmx_path_file=".\\ordered_dmx_list_99.txt"
file_type="*dmx"
dmx2sac_bat_path="c:\\sud2mseed\\dmx2sac.bat"

def dmx_sac(line):
    
    file_path,filename=line.split(";")
    filename=filename.strip()
    file_path=file_path.strip()
    sud2mseed_utils.call_dmx2sac(dmx2sac_bat_path,file_path)

"""inicio del programa"""
file_dmx=sud2mseed_utils.find_files(main_data_path,file_type, dmx_path_file)

files=open(file_dmx,'r')

Lines=files.readlines()

  
"""Ejecucion en paralelo"""
#"""
if __name__=='__main__':
    starttime=time.time()
    pool = mp.Pool(processes=12)
    results=pool.map(dmx_sac,Lines)
    pool.close()
    print("FINAL")
    print(time.time()-starttime)
#"""

"""
#Ejecucion en serie
starttime=time.time()
for line in Lines:    
    wv2dmx(line)

print(time.time()-starttime)
""" 
