
import sud2mseed_utils as utils
from datetime import timedelta,datetime
from obspy import UTCDateTime
import multiprocessing as mp
import time

def perdelta(start,end,delta):
    current=start
    while current < end:
        yield current
        current +=delta

def days_year(year):
    day_list=[]
    date_format='%Y-%m-%d %H:%M'
    date_start= "%s-01-01 00:00" %(year)
    date_end="%s-01-01 00:00" %(year + 1)

    for day in perdelta(datetime.strptime(date_start,date_format),datetime.strptime(date_end, date_format),timedelta(days=1)):
        
        day_list.append("%03d" %UTCDateTime(day).julday)
    return day_list

CPU_NUM=18

network="EC"
anio=1999
msd_folder="/datos/pcseis/sc3/1999/"
msd_file_path="./mseed_file_99.txt"
out_path="/resultado/archive/"

mseed_file=utils.find_files(msd_folder,"%s*" %network,msd_file_path)

#station_list={"ANTI_Z":["ANTI","HHZ"],"CAYA_Z":["CAYA","HHZ"]}
#98
#station_list={"ANTI_Z":["ANTI","HHZ"],"ARA2_Z":["ARA2","HHZ"],"CARM_Z":["CARM","HHZ"],"CAYA_Z":["CAYA","HHZ"],"CGGP_Z":["CGGP","HHZ"],"CHI1_Z":["CHI1","HHZ"],"COTA_Z":["COTA","HHZ"],"CUMB_Z":["CUMB","HHZ"],"CUPA_Z":["CUPA","HHZ"],"ECEN_Z":["ECEN","HHZ"],"FARH_Z":["FARH","HHZ"],"GGP_Z":["GGP","HHZ"],"GPNO_Z":["GPNO","HHZ"],"HGUL_Z":["HGUL","HHZ"],"HOJA_Z":["HOJA","HHZ"],"JAMA_Z":["JAMA","HHZ"],"JORG_Z":["JORG","HHZ"],"JUAN_Z":["JUAN","HHZ"],"JUI3_Z":["JUI3","HHZ"],"MARY_Z":["MARY","HHZ"],"MGUL_Z":["MGUL","HHZ"],"MICR_Z":["MICR","HHZ"],"MJAS_Z":["MJAS","HHZ"],"MSON_Z":["MSON","HHZ"],"NAS1_Z":["NAS1","HHZ"],"NONO_Z":["NONO","HHZ"],"PINE_E":["PINE","HHE"],"PINN_N":["PINN","HHN"],"PINO_Z":["PINO","HHZ"],"PISA_Z":["PISA","HHZ"],"QIL1_Z":["QIL1","HHZ"],"QIT2_Z":["QIT2","HHZ"],"QUR_Z":["QUR","HHZ"],"RETU_Z":["RETU","HHZ"],"RUN2_Z":["RUN2","HHZ"],"SALI_Z":["SALI","HHZ"],"SOCH_Z":["SOCH","HHZ"],"SONI_Z":["SONI","HHZ"],"TAMB_Z":["TAMB","HHZ"],"TERE_Z":["TERE","HHZ"], "TERE_E":["TERE","HHE"],"TERN_N":["TERN","HHN"],"TERV_Z":["TERV","HHZ"],"TOAZ_Z":["TOAZ","HHZ"],"VC1_Z":["VC1","HHZ"],"YANA_Z":["YANA","HHZ"]}

station_list={"ANTI_Z":["ANTI","HHZ"],"ARA2_Z":["ARA2","HHZ"],"CARS_Z":["CARS","HHZ"],"CAYA_Z":["CAYA","HHZ"],"CGGH_Z":["CGGH","HHZ"],"CGGP_Z":["CGGP","HHZ"],"CHI1_Z":["CHI1","HHZ"],"COTA_Z":["COTA","HHZ"],"CUMB_Z":["CUMB","HHZ"],"CUPA_Z":["CUPA","HHZ"],"CUSU_Z":["CUSU","HHZ"],"ECEN_Z":["ECEN","HHZ"],"FARH_Z":["FARH","HHZ"],"GGP_Z":["GGP","HHZ"],"GPNO_Z":["GPNO","HHZ"],"HOJA_Z":["HOJA","HHZ"],"IGUA_Z":["IGUA","HHZ"],"IRIG_Z":["IRIG","HHZ"],"JAMA_Z":["JAMA","HHZ"],"JORG_Z":["JORG","HHZ"],"JUA2_Z":["JUA2","HHZ"],"JUAN_Z":["JUAN","HHZ"],"JUI3_Z":["JUI3","HHZ"],"JUI4_Z":["JUI4","HHZ"],"JUI5_Z":["JUI5","HHZ"],"LORE_Z":["LORE","HHZ"],"MAGD_Z":["MAGD","HHZ"],"MARY_Z":["MARY","HHZ"],"MGUL_Z":["MGUL","HHZ"],"MIC2_Z":["MIC2","HHZ"],"MICR_Z":["MICR","HHZ"],"MSON_Z":["MSON","HHZ"],"NAS1_Z":["NAS1","HHZ"],"NONO_Z":["NONO","HHZ"],"PATA_Z":["PATA","HHZ"],"PINE_E":["PINE","HHE"],"PINN_N":["PINN","HHN"],"PINO_Z":["PINO","HHZ"],"PISA_Z":["PISA","HHZ"],"QIL1_Z":["QIL1","HHZ"],"QIT2_Z":["QIT2","HHZ"],"QUIL_Z":["QUIL","HHZ"],"QUR_Z":["QUR","HHZ"],"RETU_Z":["RETU","HHZ"],"RUN2_Z":["RUN2","HHZ"],"RUN3_Z":["RUN3","HHZ"],"SALI_Z":["SALI","HHZ"],"SOCH_Z":["SOCH","HHZ"],"SUIZ_Z":["SUIZ","HHZ"],"TAMB_Z":["TAMB","HHZ"],"TENA_Z":["TENA","HHZ"],"TERE_Z":["TERE","HHZ"], "TERE_E":["TERE","HHE"],"TERN_N":["TERN","HHN"],"TERV_Z":["TERV","HHZ"],"TLOM_Z":["TLOM","HHZ"],"TOAZ_Z":["TOAZ","HHZ"],"ULBA_Z":["ULBA","HHZ"],"VC1_Z":["VC1","HHZ"],"XTAL_Z":["XTAL","HHZ"],"YANA_Z":["YANA","HHZ"]}
def create_station_date(network,station_list,year):

    station_datetime_list=[]
    for key,station in station_list.items():

        for day in days_year(year):
            station_datetime_list.append("%s.%s..%s.%s.%s" %(network,station[0],station[1],year,day))
    
    return station_datetime_list
    
    


def process_mseed(station_data):
    
    #files_list=utils.get_mseed_day_files(mseed_file,"%s.%s..%s.%s.%s" %(station_data['network'],station_data['station'],station_data['channel'],station_data['year'],station_data['day']) )
    print("INICIO: %s" %station_data )
    files_list=utils.get_mseed_day_files(mseed_file,station_data)
    
    if len(files_list) !=0 :
        stream=utils.read_streams(files_list)   
        stream=utils.process_stream(stream)
        sc3_out_path = utils.create_output_path_sc3(out_path,stream[0])
        stream_name = utils.get_trace_name_sac(stream[0],"short")
        utils.write_sac2mseed(sc3_out_path,stream_name,stream)

lista_archivos=create_station_date("EC",station_list,anio)

"""
##Procesamiento en serie
for archivo in lista_archivos:

    process_mseed(archivo)
"""

#"""
#Ejecucion en paralelo

if __name__=="__main__":
    starttime=time.time()
    pool=mp.Pool(processes=CPU_NUM)
    results = pool.map(process_mseed,lista_archivos)
    #print(results)
    pool.close()
    print("FIN")
    print(time.time() - starttime)
#"""


