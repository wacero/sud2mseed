import pytest 
import sud2mseed_utils 
from obspy import Stream,read

import os 

def test_find_files():
    expectedfiles_list=[]
    
    main_data_path="./test/data/Net1"
    file_list_expected= os.walk(main_data_path)
    file_list_tested=sud2mseed_utils.find_files(main_data_path,"*DMX")
    print (file_list_tested)
    
    assert os.path.exists(main_data_path)
  
def test_create_output_path():
    output_path="./test/"
    output_string_expected = "%s/98/10/20" %output_path
    output_string_tested=sud2mseed_utils.create_output_path(output_path,"98102039.WVC")
    assert output_string_expected == output_string_tested
   

 
     
