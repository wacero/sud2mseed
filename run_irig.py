
import multiprocessing as mp
import sud2mseed_utils
import time

main_data_path="f:\\salida_dmx\\99"
dmx_path_file=".\\ordered_dmx_list_99.txt"
file_type="*dmx"
irig_bat_path="c:\\sud2mseed\\irig_dmx.bat"
output_path="f:\\salida_dmx"

def irig_dmx(line):
    
    file_path,filename=line.split(";")
    filename=filename.strip()
    file_path=file_path.strip()
    #out_path= sud2mseed_utils.create_output_path(output_path,filename)
    #sud2mseed_utils.call_demux_bat(demux_bat_path,file_path,filename,out_path)
    sud2mseed_utils.call_irig_dmx(irig_bat_path,file_path)

"""inicio del programa"""
file_dmx=sud2mseed_utils.find_files(main_data_path,file_type, dmx_path_file)

files=open(file_dmx,'r')

Lines=files.readlines()

"""
#Ejecucion en serie
starttime=time.time()
for line in Lines:    
    wv2dmx(line)

print(time.time()-starttime)
"""    
    
    
"""Ejecucion en paralelo"""
#"""
if __name__=='__main__':
    starttime=time.time()
    pool = mp.Pool(processes=12)
    results=pool.map(irig_dmx,Lines)
    pool.close()
    print("FINAL")
    print(time.time()-starttime)
#"""

