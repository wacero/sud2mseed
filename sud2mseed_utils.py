import shutil
import os
import fnmatch
import subprocess
import logging
import re
from datetime import datetime
from obspy import Stream,read
import numpy as np


def find_files(main_data_path, file_type):
    """
    Crea una lista de los archivos a procesar 
    
    :param string main_data_path: ruta para encontrar los archivos
    :param string file_type: string e.g. *WV*, *DMX*, *SAC* 
    :return list file_list
    """
    
    file_list = []
    
    for dir_path,dir_names, file_names in os.walk(main_data_path):
        for file in file_names:
            if fnmatch.fnmatch(file, file_type):
                #file_list.write("%s; %s\n" %(os.path.join(dir_path,file),file))
                file_list.append("%s; %s" %(os.path.join(dir_path,file),file))
   
    
    return file_list
    
def write_found_files(file_path,file_list):
    """ 
    Escrbe en un archivo de texto la lista de archivos encontrados
    
    :param string file_path: ruta de archivo creado 
    :param list file_list: lista de archivos a procesar
    :return boolean
    """
    try:
        file_object = open(file_path, 'w+')
        for line in file_list:
            file_object.write("%s\n"%line)

        file_object.close()
        return True         
    except Exception as e:
        print("ocurrio una exepcion: %s" %str(e))  
        return False      


def create_output_path(output_path, input_file_name):
    """
    Crea una ruta de carpeta basada en el nombre del archivo
    
    PCSEIS usa el estándar YYMMDDXX.WVx
    :param string output_path: ruta de salida 
    :param string input_file_name: nombre del archivo de entrada
    :return ruta del directorio
    """
    
    YY=input_file_name[0:2]
    mm=input_file_name[2:4]
    dd=input_file_name[4:6]
    
    temp_path_dir="%s\\%s\\%s\\%s" %(output_path,YY,mm,dd)
    if not os.path.isdir(temp_path_dir):
        try:
            os.makedirs(temp_path_dir)
        except Exception as e:
            pass
    
    return temp_path_dir
    #print("%s_%s_%s" %(YY,mm,dd))

def call_demux_bat(demux_bat_path,input_file_path, input_file_name,output_path):
    """
    Llama a demux para archivos WV*
    
    :param string demux_bat_path: camino de demux 
    :param string input_file_path: ruta del archivo de entrada
    :param string input_file_name: nombre de archivo de entrada
    :param string output_path: ruta de salida 
    """
    
    date_time_now= datetime.now().strftime("%Y%m%d%H%M%S_%f")
    input_file_name =input_file_name.split(".")
    output_file_name= "%s_%s.dmx" %(date_time_now,input_file_name[0])
    
    output_file_path= os.path.join(output_path,output_file_name)
    
    #subprocess.call([demux_bat_pah + " %s " %file2demux + " %s " %output_path ])
    subprocess.run([demux_bat_path, input_file_path, output_file_path ])
    
    #print(demux_bat_path, input_file_path, output_file_path)
    #print(date_time_now)

def copy_dmx(input_file_path,input_file_name,output_path):
    """
    Copia dmx para pedirlos
    
    :param string input_file_path: ruta del archivo de entrada
    :param string input_file_name: nombre de archivo de entrada
    :param string output_path: ruta de salida 
    """
    
    date_time_now= datetime.now().strftime("%Y%m%d%H%M%S_%f")
    input_file_name =input_file_name.split(".")
    output_file_name= "%s_%s.dmx" %(date_time_now,input_file_name[0])    
    output_file_path= os.path.join(output_path,output_file_name)
    shutil.copy(input_file_path,output_file_path)
    print(input_file_path,output_file_path)
    
    
def call_irig_dmx(irig_bat_path,input_file_path):
    """
    Llama a IRIG para obtener un archivo dmx
    
    :param string irig_bat_path: 
    :param string input_file_path: ruta del archivo de entrada
    """

    subprocess.run([irig_bat_path,input_file_path])
    print(input_file_path)
    
def call_dmx2sac(dmx2sac_bat_path,input_file_path):
    """
    Llama a sud2sac para obtener un archivo dxf
    
    :param string dmx2sac_bat_path: ruta dmx2sac
    :param string input_file_path: ruta del archivo de entrada
    """
    
    #subprocess.run([dmx2sac_bat_path,input_file_path])
    process = subprocess.Popen([dmx2sac_bat_path,input_file_path], stdout=subprocess.PIPE, stderr= subprocess.STDOUT)
    output,error = process.communicate()
    
    if process.returncode !=0:
        print(output)
        logging.error(input_file_path)
        #failed_dmx_file.write("%s\n" %input_file_path)
    
    
    print(input_file_path)


def get_dmx_path(original_dmx_file_path, failed_dmx_file_path,new_dmx_file_path):
    """
    Obtiene la ruta de archivos dmx
    
    :param string original_dmx_file_path: ruta del archivo dmx original
    :param string failed_dmx_file_path: ruta de archivo dmx fallida
    :param string new_dmx_file_path: nueva ruta de archivo dmx
    :return crea la nueva ruta del archivo dmx 
    """
    
    new_dmx_file=open(new_dmx_file_path, "w+")
    original_dmx_file=open(original_dmx_file_path,"r")
    failed_dmx_file=open( failed_dmx_file_path, "r")
    
    lines_failed=failed_dmx_file.readlines()
    lines_original_dmx = original_dmx_file.readlines()
    print(lines_original_dmx[:20])
    
    original_dmx=[]
    for line_f in lines_failed:
        #print(line_f)
        filename_temp,extension=line_f.split(".")
        extension.strip()
        filename=filename_temp[-8:]
        print(filename)
        res= [x for x in lines_original_dmx if re.search(filename,x)]
        if len(res) !=0:
            original_dmx.append(res[0])
    
    for line in original_dmx:
        new_dmx_file.write("".join(line)) 
       
    return new_dmx_file_path

 
def add_network(stream,network_name):
    """ 
    Agrega los datos de una red a un stream 
    
    :param stream
    :param string network_name: nombre de la red 
    :return devuelve un stream 
    
    """
    stream.stats['network' ]= network_name
    
    return stream
##Cambiar nombre a fix_channel_orientation
def fix_channel_name(stream):
    """ 
    Cambia  el nombre del canal S,V a orientacion Z para archivos sud
        
    :param: stream 
    """
        
    orientation=stream.stats['channel'].upper()
    
    if orientation == "S" or orientation == "V":
        orientation = "Z"

    if stream.stats['sampling_rate'] > 80 and stream.stats['sampling_rate']<120:
        stream.stats['channel']="SH%s" %orientation
    elif stream.stats['sampling_rate'] < 80:
        stream.stats['channel']="SH%s" %orientation
    return stream


def fix_channel_name_mseed(stream):
    """ 
    Cambia  el nombre del canal S,V a orientacion Z para archivos mseed
    
    :param: stream 
    """

    orientation=stream.stats['channel'][2].upper()

    if orientation == "S" or orientation == "V":
        orientation = "Z"

    if stream.stats['sampling_rate'] > 80 and stream.stats['sampling_rate']<120:
        stream.stats['channel']="SH%s" %orientation
    elif stream.stats['sampling_rate'] < 80:
        stream.stats['channel']="SH%s" %orientation
    return stream



def get_trace_name_sac(trace,date_format):
    """ 
    Obtiene los nombres y las fechas en fornato log o short 
    
    :param string trace:
    :param string date format: formato de la fecha 
    :return string trace_name,trace_date: 
    """

    trace_name="%s.%s.%s.%s" %(trace.stats['network'],trace.stats['station'],trace.stats['location'],trace.stats['channel'])
    trace_time=trace.stats['starttime']

    if date_format=="short":
        trace_date="%s.%03d" %(trace_time.year,trace_time.julday)
    elif date_format=="long":
        trace_date="%s.%03d.%s.%s" %(trace_time.year,trace_time.julday,trace_time.hour,trace_time.minute)

    return "%s.%s" %(trace_name,trace_date)

def create_output_path_sc3(output_path, stream):
    """
    Crea la ruta de la carpeta sc3 según el nombre del archivo
    
    :param string output_path: ruta de salida 
    :param string stream
    :return string temp_path_dir
    """
    stream_time=stream.stats['starttime']
    YYYY=stream_time.year
    network=stream.stats['network']
    station=stream.stats['station']


    temp_path_dir="%s/%s/%s/%s/" %(output_path,YYYY,network,station)
    if not os.path.isdir(temp_path_dir):
        try:
            os.makedirs(temp_path_dir)
        except Exception as e:
            pass

    return temp_path_dir

def get_mseed_day_files(mseed_file_path,station_search):
    """ 
    Obtiene los archivos mseed de una estacion por dia 
    
    :param string seed_file_path: ruta del archivo mseed 
    :param string station_search: busca una estacion 
    :return list res []: retorna una lista con archivos o una lista vacia 
    """

    mseed_file=open(mseed_file_path)
    mseed_file_lines=mseed_file.readlines()
    res = [ x for x in mseed_file_lines if re.search(station_search,x) ]

    if len(res) !=0:
        return res
    else:
        return []

def read_streams(files_list):
    """ 
    Recibe una lista de archivos esperando leerlos para fusionarlos 
    
    :param string files_list: lista de archivos 
    :return stream 
    """
    stream= Stream()
    for line in files_list:
        file_path,filename=line.split(";")
        filename=filename.strip()
        file_path=file_path.strip()
        try:
            stream +=read(file_path)
        except Exception as e:
            print("Error in read stream %s, %s" %(file_path, str(e)))

    return stream

def process_stream(stream):
    """ 
    fusiona las formas de onda 
    
    :param stream 
    :return stream 
    """

    stream.sort()
    for st in stream:
        sampling_rate=round(st.stats['sampling_rate'],0)
        if st.stats['sampling_rate'] > 80 and st.stats['sampling_rate']<120:
            st.resample(100)
        st.data=st.data.astype(np.int32)

        if st.stats['channel']=="HHH":
            st.stats['channel']="HHZ"
    try:
        stream.merge(method=1,fill_value="interpolate",interpolation_samples=0)
        return stream
    except Exception as e:
        #print("Error in merge %s " %str(e))    
        logging.error("Error in merge %s,%s" %(stream[0].stats['station'],stream[0].stats['starttime']))

def write_sac2mseed(mseed_file_path,mseed_name,stream):
    """
    Escribe un archivo sac o formato mseed
 
    :param string  mseed_file_path: ruta del archivo mseed
    :param string  mseed_name: nombre del archivo mseed 
    :param stream 
    """

    try:
        stream.write("%s/%s" %(mseed_file_path,mseed_name),format="MSEED")

    except Exception as e:
        print("Error in write_sac2mseed: %s, %s" %(mseed_name,str(e)))
        print(stream)


def write_sac2mseed_steim2(mseed_file_path,mseed_name,stream):
    """ 
    borar 
    
    :param string mseed_file_path: ruta del archivo mseed
    :param string mseed_name: nombre del archivo mseed
    :param stream 
    """

    try: 
        stream.write("%s/%s" %(mseed_file_path,mseed_name),format="MSEED",encoding="STEIM2")

    except Exception as e:
        print("Error in write_sac2mseed: %s, %s" %(mseed_name,str(e)))
        print(stream)
        




 
"""

#input_file_path="f:\\salida_dmx\\98\\11\\21\\20200826182550_198233_98112156.dmx"
process = subprocess.Popen([dmx2sac_bat_path,input_file_path], stdout=subprocess.PIPE, stderr= subprocess.STDOUT)

output,error = process.communicate()

print(output)
print("")
print(error)
print("")
print(process.returncode)

"""












