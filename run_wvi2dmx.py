
import multiprocessing as mp
import sud2mseed_utils
import time

main_data_path="e:\\pc6\\1999"
wv_path_file=".\\wvi_list_1999.txt"
file_type="*WV*"
demux_bat_path="c:\\sud2mseed\\demux_wv.bat"
output_path="f:\\salida_dmx"

def wv2dmx(line):
    
    file_path,filename=line.split(";")
    filename=filename.strip()
    file_path=file_path.strip()
    out_path= sud2mseed_utils.create_output_path(output_path,filename)
    sud2mseed_utils.call_demux_bat(demux_bat_path,file_path,filename,out_path)

"""inicio del programa"""
file_wv=sud2mseed_utils.find_files(main_data_path,file_type, wv_path_file)

files=open(file_wv,'r')

Lines=files.readlines()

"""
#Ejecucion en serie
starttime=time.time()
for line in Lines:    
    wv2dmx(line)

print(time.time()-starttime)
"""    
    
    
"""Ejecucion en paralelo"""
#"""
if __name__=='__main__':
    starttime=time.time()
    pool = mp.Pool(processes=14)
    results=pool.map(wv2dmx,Lines)
    pool.close()
    print("FINAL")
    print(time.time()-starttime)
#"""

