from datetime import datetime
import sud2mseed_utils as utils
from obspy import read
import time
import multiprocessing as mp
import logging

logging.basicConfig(format='%(message)s', filename=("./dmxsac2mseed_error_faileddmx_2000.log"),level=logging.ERROR)
input_path="/datos/pcseis/salida_dmx/00/"
#input_path="/datos/pcseis/salida_dmx/dmx_especial/98/"
out_path="/datos/pcseis/sc3"
network_name="EC"
sac_path_file="./lista_sac_corrected_2000.txt"

def sacdmx2mseed(line):
    
    file_path,filename=line.split(";")
    filename=filename.strip()
    file_path=file_path.strip()

    try:
        streams=read(file_path)
        for stream in streams:
            
            stream=utils.add_network(stream,network_name)
            stream=utils.fix_channel_name(stream)
            stream_name= "%s_%s" %( utils.get_trace_name_sac(stream,"long"),datetime.now().strftime("%f") )
            sc3_out_path = utils.create_output_path_sc3(out_path,stream)
            utils.write_sac2mseed(sc3_out_path,stream_name,stream)

            print(stream)
            logging.info("Processed stream:%s" %(file_path))
    
    except Exception as e:
        print("Error in sacdmx2mseed: %s, %s" %(file_path,str(e)))
        logging.error("Error in sacdmx2mseed: %s, %s" %(file_path,str(e)))


###Inicio del programa###

file_sac=utils.find_files(input_path,"*sac*",sac_path_file)

files=open(file_sac,'r')

Lines=files.readlines()

"""Ejecucion en paralelo"""
#"""
if __name__=='__main__':
    starttime=time.time() 
    pool = mp.Pool(processes=18)
    results=pool.map(sacdmx2mseed,Lines)
    pool.close()
    print("FINAL")
    print(time.time()-starttime)
#"""

##Ejecucion en serie
#180 vs 10 
"""
for line in Lines[:10000]:
    sacdmx2mseed(line)
"""




