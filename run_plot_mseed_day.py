import multiprocessing as mp
from obspy import Stream,read
import sud2mseed_utils as utils
import time


def plot_day_mseed(plot_path_dir, stream):

    station_name="%s.%s.%s.%s.%s" %(stream.stats['network'],stream.stats['station'],stream.stats['channel'],
                                    stream.stats['starttime'].year,stream.stats['starttime'].julday)

    stream.plot(type='dayplot',interval=60, color='k',number_of_ticks=7, linewidth=0.25,grid_linewidth=0,one_tick_per_line=True, 
                 show_y_UTC_label=True, size=(1400,900),dpi=130,title_size=10, y_label_size=6,tick_format="%H",title=station_name,
                 outfile="%s/%s.png" %(plot_path_dir,station_name) )
    print(station_name)

def run_plot(line):
    path,fil = line.split(";")
    try:
        stream=read(path.strip())
        plot_path_dir=utils.create_output_path_sc3(plot_path,stream[0])
        plot_day_mseed(plot_path_dir,stream[0])
    except Exception as e:
        print("Error in read stream: %s" %str(e) )

archive_folder="/resultado/archive/1999/"
msd_file_path="/tmp/mseed_file_1999.txt"
plot_path="/resultado/plot/"
network="EC"

#"""
mseed_file_path=utils.find_files(archive_folder,"%s*" %network,msd_file_path)

mseed_file=open(mseed_file_path,'r')

Lines=mseed_file.readlines()

#"""
#ejecucion en paralelo

if __name__=='__main__':
    starttime=time.time()
    pool = mp.Pool(processes=16)
    results=pool.map(run_plot,Lines)
    pool.close()
    print("FINAL")
    print(time.time()-starttime)
#"""




"""
#ejecucion en serie
for line in Lines:
    print(line)
    path,fil = line.split(";")
    try:
        stream=read(path.strip())
        plot_path_dir=utils.create_output_path_sc3(plot_path,stream[0])
        plot_day_mseed(plot_path_dir,stream[0])
    except Exception as e:
        print("Error in read stream: %s" %str(e) )

"""



