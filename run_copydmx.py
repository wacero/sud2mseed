import sud2mseed_utils
import multiprocessing as mp
import time

#COPIAR ARCHIVOS DMX Y ORDENAR. 

#crear la lista de archivos dmx
#crear las carpetas 
#copiar a las carpetas 

CPU_NUM=14
main_data_path="e:\\pc6\\1999\\"
dmx_path_file=".\\dmx_list_99.txt"
file_type="*DMX"
output_path="f:\\salida_dmx\\"


def copy_dmx(line):

    file_path,filename=line.split(";")
    filename=filename.strip()
    file_path=file_path.strip()
    out_path= sud2mseed_utils.create_output_path(output_path,filename)
    sud2mseed_utils.copy_dmx(file_path,filename,out_path)


"""Inicio del programa"""
file_dmx=sud2mseed_utils.find_files(main_data_path, file_type, dmx_path_file )

files=open(file_dmx,'r')

Lines = files.readlines()

"""Ejecucion en serie"""
"""
starttime=time.time()
for line in Lines:
    copy_dmx(line)
print(time.time() - starttime) 
"""

"""Ejecucion en paralelo"""

#"""
if __name__=="__main__":
    starttime=time.time()
    pool=mp.Pool(processes=CPU_NUM)
    results = pool.map(copy_dmx,Lines)
    #print(results)
    pool.close()
    print("FIN")
    print(time.time() - starttime)
#""" 
    
    